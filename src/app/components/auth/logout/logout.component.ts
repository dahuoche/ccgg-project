import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../shared/services/auth.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.css']
})
export class LogoutComponent implements OnInit {
  private response: any;

  constructor(public authService: AuthService,
              private router: Router) { }

  ngOnInit() {
    this.authService.checklogin()
      .subscribe((res) => {
        this.response = res;
        console.log(res);
        if (res.success) {
          this.router.navigate(['/logout']);
        }
      });
  }
  logout() {
    this.authService.logout()
      .subscribe((res) => {
        this.response = res;
        if (res.success) {
          // this.router.navigate(['/']);
        }
      });
  }

}
