import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {LoginComponent} from './components/auth/login/login.component';
import {AppGuard} from './app.guard';
import {RegisterComponent} from './components/auth/register/register.component';
import {WelcomeComponent} from "./components/welcome/welcome.component";
import {LogoutComponent} from "./components/auth/logout/logout.component";

const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full',
  },
  {
    path: '',
    canActivate: [AppGuard],
    children: [
      {
        path: 'welcome',
        component: WelcomeComponent
      },
      {
        path: 'logout',
        component: LogoutComponent
      }
      ]
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'register',
    component: RegisterComponent
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule {
}
